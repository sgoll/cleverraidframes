-- Using locals for faster execution
local _G = _G
local gsub, strsub = gsub, strsub

local ShouldShowName = ShouldShowName
local GetUnitName = GetUnitName

local UnitGroupRolesAssigned = UnitGroupRolesAssigned

local UnitIsConnected = UnitIsConnected
local UnitIsAFK = UnitIsAFK
local UnitIsGhost = UnitIsGhost
local UnitIsPossessed = UnitIsPossessed
local UnitIsCharmed = UnitIsCharmed
local UnitIsFeignDeath = UnitIsFeignDeath

-- use addon namespace for easy access across modules
local name, addon = ...
addon.crf = LibStub("AceAddon-3.0"):NewAddon(name, "AceHook-3.0", "AceEvent-3.0", "AceBucket-3.0", "AceConsole-3.0")
local crf = addon.crf

-- OnInitialize
-- Called directly after addon is fully loaded
function crf:OnInitialize()
    -- empty, this is where we would setup AceDB and options
    -- handling
end

-- OnEnable
-- Called when addon is manually enabled by the user and
-- if enabled already, after PLAYER_LOGIN
function crf:OnEnable()

    -- debug
    self:Print("Raid frame enhancements enabled.")

    -- Larger Frame Sliders
    local h = _G["CompactUnitFrameProfilesGeneralOptionsFrameHeightSlider"]
    local w = _G["CompactUnitFrameProfilesGeneralOptionsFrameWidthSlider"]
    h:SetMinMaxValues(10, 75)
    w:SetMinMaxValues(10, 175)

    -- Hook Secure Functions
    self:SecureHook("CompactUnitFrame_UpdateName")
    self:SecureHook("CompactUnitFrame_UpdateRoleIcon")
    self:SecureHook("CompactUnitFrame_UpdateStatusText")

end

-- OnDisable
-- Called when addon is manually disabled by the user
function crf:OnDisable()
    -- empty, Ace3 does some automagic crap in here
end

-- CompactUnitFrame_UpdateName
-- This is a secure hook to Blizzard's CompactUnitFrame_UpdateName
-- function
-- It removes realm names from unit frames, resizes the name
-- and repositions it
function crf:CompactUnitFrame_UpdateName(frame)

    -- debug
    -- self:Print("CompactUnitFrame_UpdateName called")

    -- make sure it's safe to edit the unit frame
    if frame:IsForbidden() then
        
        -- debug
        -- self:Print("Forbidden frame in CompactUnitFrame_UpdateName hook")
        return 

    end

    if not ShouldShowName(frame) then
        return
    end

    -- get player name, hide realm, and shorten to 10 characters
    local name = frame.name
    local player = GetUnitName(frame.unit, true)

    if (player) then
        local shortName = gsub(player, "%-[^|]+", "")
        shortName = strsub(shortName, 1, 10)
        name:SetText(shortName)
    end

    -- scale player name to be larger
    name:SetScale(1.15)

    -- reposition the name
    name:ClearAllPoints()
    name:SetPoint("TOPLEFT", 5, -4)

end

-- CompactUnitFrame_UpdateRoleIcon
-- This is a secure hook to Blizzard's CompactUnitFrame_UpdateRoleIcon
-- function
-- It repositions and resizes the icon, also hides all damage icons in
-- a group, leaving just healer and tank icons
function crf:CompactUnitFrame_UpdateRoleIcon(frame)

    -- debug
    -- self:Print("CompactUnitFrame_UpdateRoleIcon called")

    -- make sure it's safe to edit the unit frame
    if frame:IsForbidden() then

        -- debug
        -- self:Print("Forbidden frame in CompactUnitFrame_UpdateRoleIcon hook")
        return

    end

    local icon = frame.roleIcon

    if not icon then
        return
    end

    icon:SetScale(1.0)

    -- reposition and scale the icon
    icon:ClearAllPoints()
    icon:SetPoint("TOPRIGHT", -5, -5)

    -- hide all dps icons
    local role = UnitGroupRolesAssigned(frame.unit)

    if role == "DAMAGER" then
        icon:Hide()
    end

end

-- CompactUnitFrame_UpdateStatusText
-- This is a secure hook to Blizzard's CompactUnitFrame_UpdateStatusText
-- function
-- It adds functionality for multiple statuses beyond what default frames
-- normally show
function crf:CompactUnitFrame_UpdateStatusText(frame)

    -- debug
    -- self:Print("CompactUnitFrame_UpdateStatusText called")

    -- make sure it's safe to edit the unit frame
    if frame:IsForbidden() then

        -- debug
        -- self:Print("Forbidden frame in CompactUnitFrame_UpdateStatusText hook")
        return

    end

    local status = frame.statusText

    -- make sure there's some statusText to work on
    if not status then
        return
    end

    if not frame.optionTable.displayStatusText then
        return
    end

    local unit = frame.unit
    local dUnit = frame.displayedUnit

    -- show default offline status over all other statuses
    if not UnitIsConnected(unit) then
        return
    end

    -- custom statuses
    if UnitIsAFK(unit) then
        status:SetText("AFK")
        status:Show()
    elseif UnitIsGhost(dUnit) then
        status:SetText("Ghost")
        status:Show()
    elseif UnitIsPossessed(dUnit) then
        status:SetText("Possessed")
        status:Show()
    elseif UnitIsCharmed(dUnit) then
        status:SetText("Charmed")
        status:Show()
    elseif UnitIsFeignDeath(dUnit) then
        status:SetText("Feign")
        status:Show()
    end

end